﻿using BluetoothConfigurationApp.Persistence;
using Plugin.BLE.Abstractions.Contracts;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BluetoothConfigurationApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FileTransferPage : ContentPage
    {
        Guid SerialCommunicationServiceGuid = new Guid("57b53956-da15-4005-8a1f-fcacde09af51");
        Guid ServerToClientCharacGuid = new Guid("9ecadb3b-498c-4c15-ac0e-350aa3a148d5");
        Guid ClientToServerCharacGuid = new Guid("68c6f92c-ead2-4b70-a6c0-20f6a41622f4");

        ICharacteristic ServerToClientCharacteristic = null;
        ICharacteristic ClientToServerCharacteristic = null;
        byte[] SendDataArray = new byte[250];

        Stopwatch stopwatch = new Stopwatch();
        int TransferCountBytes = 0;
        IDevice ConnectedDevice;
        public FileTransferPage(IDevice connectedDevice)
        {
            InitializeComponent();
            ConnectedDevice = connectedDevice;
        }

        protected async override void OnAppearing()
        {
            base.OnAppearing();
            var services = await ConnectedDevice.GetServicesAsync();
            var serialCommServices = await ConnectedDevice.GetServiceAsync(SerialCommunicationServiceGuid);
            if(serialCommServices != null)
            {
                ServerToClientCharacteristic = await serialCommServices.GetCharacteristicAsync(ServerToClientCharacGuid);
                ClientToServerCharacteristic = await serialCommServices.GetCharacteristicAsync(ClientToServerCharacGuid);
            }

        }

        private void SetTransferSpeedLabel(int speed)
        {
            TransferSpeedLabel.Text = "Transfer speed: " + speed + " kbps";
        }

        private void SetTransferPercentLabel(int percent)
        {
            TransferPercentLabel.Text = percent + "% Complete";
        }

        private void SetElapsedTimeLabel(int elapsedTime)
        {
            TimeElapsedLabel.Text = "Elapsed Time: " + elapsedTime + " Seconds";
        }

        private async void StartServerToClientTransferButton_Clicked(object sender, EventArgs e)
        {
            if(ServerToClientCharacteristic.CanRead)
            {
                byte[] dataReceived = await ServerToClientCharacteristic.ReadAsync();
            }
        }

        private async void StartClientToServerTransferButton_Clicked(object sender, EventArgs e)
        {
            if(ClientToServerCharacteristic.CanWrite)
            {
                bool result = await ClientToServerCharacteristic.WriteAsync(SendDataArray);
                if(result)
                {
                    DependencyService.Get<IToastMessage>().ShortAlert("Characteristic write success");
                }
                else
                {
                    DependencyService.Get<IToastMessage>().ShortAlert("Characteristic write fail");
                }
            }
        }
    }
}