﻿using Plugin.BLE.Abstractions.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Plugin.BLE.Abstractions;
using Plugin.BLE.Abstractions.EventArgs;
using System.Threading;

namespace BluetoothConfigurationApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CharacteristicsPage : ContentPage
    {
        IList<ICharacteristic> CharacteristicsList;
        public CharacteristicsPage(IList<ICharacteristic> characteristicsList)
        {
            InitializeComponent();
            BindingContext = this;
            CharacteristicsList = characteristicsList;
            
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            listView.ItemsSource = CharacteristicsList;
            
        }

        private async void listView_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            var charac = e.Item as ICharacteristic;
            await Navigation.PushAsync(new EditCharacteristicPage(charac));
            listView.SelectedItem = null;
        }

    }
}