﻿using Plugin.BLE.Abstractions.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BluetoothConfigurationApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ServiceListPage : ContentPage
    {
        IList<IService> ServicesList;
        public ServiceListPage(IList<IService> serviceList)
        {
            InitializeComponent();
            BindingContext = this;
            ServicesList = serviceList;
       
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            listView.ItemsSource = ServicesList;
        }

        private async void listView_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            var service = e.Item as IService;
            var characteristics = await service.GetCharacteristicsAsync();
            await Navigation.PushAsync(new CharacteristicsPage(characteristics));
            listView.SelectedItem = null;
        }
    }
}