﻿using Plugin.BLE.Abstractions.Contracts;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BluetoothConfigurationApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SelectTestPage : ContentPage
    {
        public ObservableCollection<string> Items { get; set; }
        private IDevice ConnectedDevice;
        public SelectTestPage(IDevice connectedDevice)
        {
            InitializeComponent();

            Items = new ObservableCollection<string>
            {
                "Pairing Test",
                "File Transfer Speed Test",
                "Trigger IO Test",
            };

            BindingContext = this;
            ConnectedDevice = connectedDevice;
        }

        async void Handle_ItemTapped(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem == null)
                return;

            if(((ListView)sender).SelectedItem.ToString() == "Pairing Test")
            {
                await Navigation.PushAsync(new PairingTestPage(ConnectedDevice));
            }
            else if(((ListView)sender).SelectedItem.ToString() == "File Transfer Speed Test")
            {
                await Navigation.PushAsync(new FileTransferPage(ConnectedDevice));
            }
            else if(((ListView)sender).SelectedItem.ToString() == "Trigger IO Test")
            {

            }


            //Deselect Item
            ((ListView)sender).SelectedItem = null;
        }
    }
}