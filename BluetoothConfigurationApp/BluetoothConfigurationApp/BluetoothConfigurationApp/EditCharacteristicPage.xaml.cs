﻿using BluetoothConfigurationApp.Persistence;
using Plugin.BLE.Abstractions.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BluetoothConfigurationApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EditCharacteristicPage : ContentPage
    {
        ICharacteristic Characteristic;
        public EditCharacteristicPage(ICharacteristic characteristic)
        {
            InitializeComponent();
            //BindingContext = this;
            Characteristic = characteristic;
            
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            var CharacteristicValue = await Characteristic.ReadAsync();
            var characteristicString = Encoding.UTF8.GetString(CharacteristicValue, 0, CharacteristicValue.Length);
            DependencyService.Get<IToastMessage>().LongAlert("Characteristic read value: " + characteristicString);
            characteristicLabel.Text = characteristicString;
        }

        private async void Change_Button_Clicked(object sender, EventArgs e)
        {
            var EnteredText = editCharacteristicEntry.Text;
            if(EnteredText.Length > 0)
            {
                var CharacteristicValue = Encoding.UTF8.GetBytes(EnteredText);
                if (Characteristic.CanWrite)
                {
                    var result = await Characteristic.WriteAsync(CharacteristicValue);
                    if(result)
                        DependencyService.Get<IToastMessage>().LongAlert("Characteristic write successful");
                    else
                        DependencyService.Get<IToastMessage>().LongAlert("Characteristic write unsuccessful");

                }
            }

        }
    }
}