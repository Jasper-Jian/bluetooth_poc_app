﻿using BluetoothConfigurationApp.Persistence;
using Plugin.BLE;
using Plugin.BLE.Abstractions.Contracts;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BluetoothConfigurationApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainPage : ContentPage
    {

        IBluetoothLE ble;
        IAdapter adapter;
        IDevice ConnectedDevice;
        ObservableCollection<IDevice> deviceList = new ObservableCollection<IDevice>();
        bool showServiceList;

        public MainPage()
        {
            InitializeComponent();
            BindingContext = this;

            ble = CrossBluetoothLE.Current;
            adapter = CrossBluetoothLE.Current.Adapter;
            adapter.DeviceDiscovered += OnDeviceDiscovered;
            adapter.DeviceConnected += OnDeviceConnected;
            adapter.DeviceDisconnected += (o, e) => { Xamarin.Forms.Device.BeginInvokeOnMainThread(() => DependencyService.Get<IToastMessage>().ShortAlert("Device Disconnected")); };
            adapter.ScanTimeoutElapsed += (o, e) => { Xamarin.Forms.Device.BeginInvokeOnMainThread(() => DependencyService.Get<IToastMessage>().ShortAlert("Scanning Timed Out")); };
            adapter.ScanMode = ScanMode.Balanced;
            adapter.ScanTimeout = 30000;


        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            listView.ItemsSource = deviceList;
            showServiceList = false;

        }

        private void OnDeviceDiscovered(Object source, Plugin.BLE.Abstractions.EventArgs.DeviceEventArgs e)
        {
            deviceList.Add(e.Device);
            
        }

        private void StartScan_Button_Clicked(object sender, EventArgs e)
        {
            deviceList.Clear();
            listView.ItemsSource = deviceList;
            var result = adapter.StartScanningForDevicesAsync();
        }

        private void StopScan_Button_Clicked(object sender, EventArgs e)
        {
            var result = adapter.StopScanningForDevicesAsync();
            listView.ItemsSource = deviceList;
        }

        private void listView_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            var device = e.Item as IDevice;
            //Connect to device 
            var result = adapter.ConnectToDeviceAsync(device);

            listView.SelectedItem = null;

        }

        private void ViewServicesMenuItem_Clicked(object sender, EventArgs e)
        {
            showServiceList = true;
            var device = (sender as MenuItem).CommandParameter as IDevice;
            var result = adapter.ConnectToDeviceAsync(device);
        }

        private async void OnDeviceConnected(object source, Plugin.BLE.Abstractions.EventArgs.DeviceEventArgs e)
        {
            ConnectedDevice = e.Device;
            var services = await e.Device.GetServicesAsync();
            if(showServiceList)
            {
                Xamarin.Forms.Device.BeginInvokeOnMainThread(() => Navigation.PushAsync(new ServiceListPage(services)));
            }
            else
            {
                Xamarin.Forms.Device.BeginInvokeOnMainThread(() => Navigation.PushAsync(new SelectTestPage(ConnectedDevice)));
            }


        }

        private void Disconnect_Button_Clicked(object sender, EventArgs e)
        {
            adapter.DisconnectDeviceAsync(ConnectedDevice);
        }
    }
}
