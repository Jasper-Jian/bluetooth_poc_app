﻿
using Foundation;
using UIKit;
using BluetoothConfigurationApp.iOS.Persistence;
using BluetoothConfigurationApp.Persistence;

[assembly: Xamarin.Forms.Dependency(typeof(ToastMessageIOS))]
namespace BluetoothConfigurationApp.iOS.Persistence
{
    public class ToastMessageIOS : IToastMessage
    {
        const double LONG_DELAY = 2.7;
        const double SHORT_DELAY = 1.3;

        NSTimer alertDelay;
        UIAlertController alert;

        public void LongAlert(string message)
        {
            ShowAlert(message, LONG_DELAY);
        }
        public void ShortAlert(string message)
        {
            ShowAlert(message, SHORT_DELAY);
        }

        void ShowAlert(string message, double seconds)
        {
            alertDelay = NSTimer.CreateScheduledTimer(seconds, (obj) =>
            {
                dismissMessage();
            });
            alert = UIAlertController.Create(null, message, UIAlertControllerStyle.Alert);
            UIApplication.SharedApplication.KeyWindow.RootViewController.PresentViewController(alert, true, null);
        }

        void dismissMessage()
        {
            if (alert != null)
            {
                alert.DismissViewController(true, null);
            }
            if (alertDelay != null)
            {
                alertDelay.Dispose();
            }
        }
    }
}